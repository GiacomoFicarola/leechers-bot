To be able to use this library, you need to enable Google Custom Search API, generate API key credentials and set a project:

    Visit https://console.developers.google.com and create a project.

    Visit https://console.developers.google.com/apis/library/customsearch.googleapis.com and enable "Custom Search API" for your project.

    Visit https://console.developers.google.com/apis/credentials and generate API key credentials for your project.

    Visit https://cse.google.com/cse/all and in the web form where you create/edit your custom search engine enable "Image search" option and for "Sites to search" option select "Search the entire web but emphasize included sites".

After setting up your Google developers account and project you should have been provided with developers API key and project CX.
