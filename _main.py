from tinydb import TinyDB, Query
from tinydb.storages import JSONStorage
from tinydb.middlewares import CachingMiddleware
from apscheduler.schedulers.background import BackgroundScheduler
from pyrogram import Client, filters
from pyrogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from pyrogram.errors import FloodWait
from bs4 import BeautifulSoup
from google_images_search import GoogleImagesSearch
from datetime import datetime
import json
import requests
import re
import time

gis = GoogleImagesSearch('google-api-key', 'google-cx-id')

api_id=""
api_hash=""
bot_token=""

CHAT = 999999999999
ENDPOINT = {
    "user": "https://1337x.to/user/{username}/",
    "torrents": "https://1337x.to/{username}-torrents/1/",
    "release": "https://1337x.to/{url}"
}
N_CHECKS = 0

app = Client(
    "my_bot",
    api_id=api_id, api_hash=api_hash,
    bot_token=bot_token
)

db = TinyDB('db.json', storage=JSONStorage)
scheduler = BackgroundScheduler()
    
@app.on_message(filters.command("add"))
def add(client, message):
    if message.chat.id == CHAT:
        leecher = message.text.lstrip("/add").strip()
        if " " in leecher:
            client.send_message(message.chat.id, "Wrong username")
            return
        r = requests.get(ENDPOINT["user"].format(username=leecher))
        if r.status_code == 404:
            client.send_message(message.chat.id, "Leecher does not exsist")
            return
        Leech = Query()
        r = db.search(Leech.name == leecher)
        if not r:
            db.insert({"name": leecher, "data": []})
            client.send_message(message.chat.id, "Leecher added")
        else:
            client.send_message(message.chat.id, "Leecher already added")

@app.on_message(filters.command("remove"))
def remove(client, message):
    if message.chat.id == CHAT:
        leecher = message.text.lstrip("/remove").strip()
        if " " in leecher:
            client.send_message(message.chat.id, "Wrong username")
            return
        if leecher == "tony":
            client.send_message(message.chat.id, "YOU CAN'T TOUCH TONY!!")
            return
        Leech = Query()
        r = db.search(Leech.name == leecher)
        if not r:
            client.send_message(message.chat.id, "Leecher not present")
        else:
            db.remove(Leech.name == leecher)
            client.send_message(message.chat.id, "Leecher removed")

@app.on_message(filters.command("list"))
def list(client, message):
    if message.chat.id == CHAT:
        msg = "<b>All leechers: </b>\n"
        for el in db.all():
            el = json.loads(json.dumps(el))
            msg += f"[{el.get('name')}]({ENDPOINT['user'].format(username=el.get('name'))}), "
        client.send_message(message.chat.id, msg.rstrip(", "), disable_web_page_preview=True)

def get_poster_url(name):
    _search_params = {
        'q': name,
        'num': 10,
        'safe': "off",
        'fileType': 'jpg|png',
        'imgSize': 'large',
        'imgType': 'photo'
    }
    
    gis.search(search_params=_search_params)
    return gis.results()[0].url

def _scrape_handler():
    global N_CHECKS
    print(f"[{datetime.now().strftime('%d-%m-%Y at %H:%M:%S')}] Scheduler >> Check {N_CHECKS}")
    N_CHECKS += 1
    scrape()

def scrape():
    leechers = db.all()
    for i,el in enumerate(leechers):
        el = json.loads(json.dumps(el))
        r = requests.get(ENDPOINT['torrents'].format(username=el.get('name')))
        msg = app.send_message(CHAT, f"Scanning {el.get('name')} ({i+1}/{len(leechers)})", disable_notification=True)
        soup = BeautifulSoup(r.content, 'html.parser')
        if soup.find("table"):
            rows = soup.find("table").find("tbody").find_all("tr")
            Leech = Query()
            for row in rows[::-1]:
                url = row.find_all("a")[1].get("href")
                t = row.find_all("td")[-1].text
                name = re.sub("\/\w*\/\d*\/", "", url).replace("-", " ").strip("/")
                entry = {"name": name,"url": url, "time": t}
                if entry.get('url') not in [url.get('url') for url in el.get("data")]:
                    el.get("data").append(entry)
                    db.update({"data": el.get("data")}, Leech.name == el.get("name"))
                    button = InlineKeyboardMarkup([[InlineKeyboardButton("Go to release", url=ENDPOINT['release'].format(url=url.lstrip("/")))]])
                    try:
                        try:
                            poster_url = get_poster_url(f"poster {name[:20]}")
                            app.send_photo(CHAT, poster_url, caption=f"❕ <b>New torrent release from</b> [{el.get('name')}]({ENDPOINT['user'].format(username=el.get('name'))})\n\n<b>🎬 Title</b>: {name}\n\n🕐 <b>Eta</b>: {t}", reply_markup=button)
                        except:
                            app.send_message(CHAT, f"❕ <b>New torrent release from</b> [{el.get('name')}]({ENDPOINT['user'].format(username=el.get('name'))})\n\n<b>🎬 Title</b>: {name}\n\n🕐 <b>Eta</b>: {t}", reply_markup=button, disable_web_page_preview=True)
                    except FloodWait as e:
                        print(e)
                        time.sleep(e.value)
                    time.sleep(5)
        time.sleep(5)
        msg.delete()

if __name__ == "__main__":
    scheduler.add_job(_scrape_handler, 'interval', minutes=60)
    scheduler.start()
    app.run()
    
