#definitivo
from datetime import datetime
from typing import cast
import botogram
import requests
import feedparser
import re
from bs4 import BeautifulSoup
from tinydb import TinyDB, Query
db_hd = TinyDB('db_hd.json')
db = TinyDB('db.json')
leecher = TinyDB('leecher.json')
urllink = 'https://1337x.to'

proxy = {"http":"socks4://185.97.121.197:4153/",
        "https":"socks4://185.97.121.197:4153/"}

bot = botogram.create("xxxyyy")


@bot.command("lenght")
def hello_command(chat):
    chat.send('il numero di elementi presenti nel database è: ' + str(len(db)))


@bot.command("insert")
def insert_leecher(chat, message):
    if message.reply_to_message is None:
        return
    leecher.insert({"leecher": message.reply_to_message.text})
    chat.send("leecher added")


@bot.command("delete")
def del_user(chat, message):
    leecherq = Query()
    leecherk_replaced = None
    if chat.id == 99999999:
        ml = message.parsed_text.filter("plain")
        if ml == []:
            chat.send("Errore! Devi scrivere il nome del leecher da eliminare... stupido")
            return
        for element in ml:
            leecherk = element.text
            leecherk_replaced = leecherk.replace(" ", "")
        db_query = leecher.search(leecherq.leecher == leecherk_replaced)
        if db_query != []:
            leecher.remove(leecherq.leecher == leecherk_replaced)
            chat.send("rimosso "+leecherk_replaced)
        else:
            chat.send("Errore! Leecher non trovato")
    else:
       chat.send("non hai i permessi necessari per usare questo comando :()")


@bot.command("list")
def print_list(chat):
    leechers = leecher.all()
    j = 0
    btn = botogram.Buttons()
    for element in leechers:
        btn[j].url(element['leecher'], "https://1337x.to/user/" +
                   element['leecher'] + "/")
        j += 1
    chat.send("La lista dei leecher:", attach = btn)


@bot.command("scan")
def scan(bot):
    bot.chat(9999999999).send("Avvio scansione manuale!")
    webScraping(bot)


@bot.timer(3600)  # eseguito ogni 1h
def scan_timer(bot):
    webScraping(bot)


def webScraping(bot):
    leechers = leecher.all()
    for name in leechers:
        url = urllink+'/user/' + name['leecher'] + '/'
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')
        value = soup.select('tr')
        for element in value[::-1]:
            for td in element.select('td.coll-1'):
                eta = element.select('td.coll-5')
                for date in eta:
                    time = date.get_text()
                for link in td.find_all(href = re.compile('torrent')):
                    if db.search(Query()['link'] == urllink+link.get('href')) == []:
                        db.insert({'uploader': name['leecher'], 'link': urllink + link.get('href')})
                        bot.chat(99999999999999).send("❕Nuovo Torrent rilasciato da <a href='" + url + "'>" + name['leecher'] + "</a>.\n\n➡️ <a href='" + urllink + link.get(
                            'href') + "'>" + link.get_text() + "</a>\n\n🕐 Eta:" + time, preview = False, syntax = 'html')
    hdtorrents()

@bot.command("log")
def log(bot):
    bot.chat(9999999999999).send_file(path="log.txt")

@bot.command("logme")
def logme(bot,chat):
    chat.send_file(path="log.txt")

def hdtorrents():
    f = open("log.txt", "a")
    x = datetime.now()
    try:
        feed = feedparser.parse(requests.get("http://www.hdtorrents.xyz/rss.php?cat=3,4,1,2,7&passkey=911cc34b02d9198b4e26382666b6732d",proxies=proxy).text)
    except:
        f.write(str(x.year)+"-"+str(x.month)+"-"+str(x.day)+" "+str(x.hour)+":"+str(x.minute)+":"+str(x.second)+"\n")
        f.close()
        return
    for element in feed['entries'][::-1]:
        if db_hd.search(Query()['link'] == element['link']) == []:
            db_hd.insert({'link': element['link']})
            message = "<a href='" + \
            element['link'] + "'>" + element['title'] + "</a>\n\n" + element['summary']
            message_replaced = message.replace("&nbsp;", "")
            bot.chat(9999999999).send(message_replaced,preview = False, syntax = "html")


if __name__ == "__main__":
        bot.run()
